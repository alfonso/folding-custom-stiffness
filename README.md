# ACCESS TO THE PDF PROPOSAL [HERE](Alfonso_Parra__Covid19_Spring_2020_Independent_Study__1_.pdf)



# **Independent Studies Proposal. Spring 2020.** Folding Custom Stiffness

The study of curved crease patterns is the state of the art in computational Origami. Higher accessibility to the algorithms that generate, analyze, and represent these geometries combined with the intrinsic beauty of transforming a planar sheet into a desired three-dimensional shapes is the cause of its attention.


Shape is sometimes an underestimated factor that contributes with high importance to the structural integrity of a solution. It is responsible of distributing homogeneously the energy that a fixed mass geometry can handle. When working on situations in which the means of manufacturing, the material, and the raw material are fixed, in order to maximize performance, shape is the most responsible variable to iterate with. 

The MIT CBA Covid-19 response has been and is a challenging collage of projects in which we had to deliver solutions as fast, as better and in larger quantities as possible. Since the beginning with the face shield project, folding has been a great choice. Simplifies the manufacturing to a 2D process, can be used to locally increase rigidity provide shape. But scaling up models in size maintaining thickness is a factor that decreases stiffness of the geometry. 

In this independent study, it is wanted to analyze and quantify how much curved crease patterns increase the rigidity of thin-shell structure under a fixed load. Also, as a proof of concept, the COVID-19 Isolation Box will be shown.


## Background Material


The base of this research lies on previous work on fold structures representation and energy study. Some of the paper / theses /classes I will consult are: 



*  Sam Calisch work on Folded Functional Foams.  (http://cba.mit.edu/docs/theses/19.09.calisch.pdf)


*  David Huffman’s approach to Computational Design with Curved Creased. (https://courses.csail.mit.edu/6.849/spring17/lectures/C05-images.pdf)

*  Erik Demaine MIT 6.849 Class Content (http://courses.csail.mit.edu/6.849/spring17/)

## Study Process

In order to be able to analyze the problem, the proposed approach to study this geometry will be:
* **Research on reflection geometries**
David Huffman approach to curve creased structures as a planar refection of a simple curvature surface. Possibilities to transform any other basic tessellation. 

* **Study the curved crease transformation of the Miura Ori tessellation**
Analysis of the ODE equation that governs the shape in function of the angle and curvature. Generate a workflow to make a library of samples controlling the parameters changed.

* **FEM analysis of the samples library**
Structural analysis on static load and bucking to all the library generated.  Complete the library by adding FEM results.  

* **Analysis and design space**
Once generated the characteristics of the library, understand the design space and take conclusions. 

* **Direct application.**
Design an structural optimized isolation box. Quantitative comparison vs. a same shape without curved creases.



## Desired Output 
As a final result this study wants to quantify and analyze the following questions: 
* Buckling and static stress study on simple hinges vs. curved creased folded geometries.
* Parameters of curve creased folds, its relations and role with its structural output behaviour under load.


## Evaluation 

The evaluation of this proposal can be driven by the clarity of the analysis done, the approach taken and the quality of the studies.